
package Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import Middle.Message;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author batman
 */
public class Client implements Runnable {
	
	private Socket cliente;

	
	public Client (Socket cliente){
	
		this.cliente = cliente;

	}

	 public static void main(String args[]) throws UnknownHostException, IOException{
	 
	
	Socket sock = new Socket ("localhost", 6688);
	
	Client cli = new Client (sock);
	Thread multi = new Thread (cli);
	
	multi.start();
	 
	}
	
        @Override
         public void run(){
//public void executar (){
		try {
			Message mensagem;
			System.out.println("Cliente Conectou ao Servidor");
			
			//Scanner teclado = new Scanner(cliente.getInputStream());
                        Scanner teclado = new Scanner(System.in);
                        /*
                        //objeto para enviar msg;
			ObjectOutputStream oOutput = new ObjectOutputStream(cliente.getOutputStream());
			
			//Envia msg;
			while (teclado.hasNextLine()){
				oOutput.println(teclado.nextLine())
			}
			
			mensagem = (Message) oOutput.readObject();
                            */
			//objeto para enviar msg;
			ObjectOutputStream oOutput = new ObjectOutputStream(cliente.getOutputStream());
                        ObjectInputStream oInput = new ObjectInputStream(cliente.getInputStream());
                      
                        //Envia msg;
			while (teclado.hasNextLine()){
				oOutput.writeObject(teclado.nextLine());
                        }
                        
                        oOutput = (ObjectOutputStream) oInput.readObject();
			
			mensagem = (Message) oInput.readObject();

			oInput.close();
                        oOutput.close();
			teclado.close();
			cliente.close();
			System.out.println("Fim do Cliente!");
		}catch (IOException e){
		} catch (ClassNotFoundException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
	}
}

