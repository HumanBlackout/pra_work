/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Middle.Message;

/**
 *
 * @author batman
 */
public class CommandFactory {
    
    private CommandFactory() {
		
	}
	
	public static Command getCommand( Message mensagem ) {
		if( mensagem.isDel() ) {
			return new DelCommand(mensagem);
		}
		if( mensagem.isFind() ) {
			return new FindCommand(mensagem);
		}
                if (mensagem.isAdd()){
                    return new AddCommand(mensagem);
                }
		return new QuitCommand(mensagem);
	}

}
