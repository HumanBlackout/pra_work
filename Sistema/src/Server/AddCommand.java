/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Middle.Message;
import Middle.Vetor_BTree;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author batman
 */
public class AddCommand extends Command{
	private Message message;
        private Vetor_BTree key;
        
	public AddCommand( Message mensagem ) {
		message = mensagem;
	}
        
        @Override
	public void run() {
            try {
                key.AddNode();
                //dados.deleteNode(Integer.parseInt(this.message));
            } catch (IOException ex) {
                Logger.getLogger(AddCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
            
	}
}
