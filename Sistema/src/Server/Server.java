/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import Middle.Message;

/**
 *
 * @author batman
 */
public class Server {
    
    private static final int PORT = 6688;
	private static final Logger LOG = Logger.getLogger(Server.class.getCanonicalName()); 
	
	public Server() {
		
	}
	
	private class ClientManager implements Runnable {
		// Endereco do cliente atual
		private final Socket cliente;
		
		public ClientManager(Socket origem) {
			cliente = origem;
			
		}
		
                @Override
		public void run() {
			try {
				receiveMessages();
			} catch (ClassNotFoundException | IOException e) {
			   LOG.severe("Problema na conex�o com o cliente");
			} finally {
		        try {
					cliente.close();
				} catch (IOException e) {
					LOG.severe("Problema ao fechar a conex�o");
				}
			}
		}
		
		private void receiveMessages() throws ClassNotFoundException, IOException {
			Message mensagem;
			do {
				LOG.info("Recebendo mensagem");
				ObjectInputStream oInput = new ObjectInputStream(cliente.getInputStream());
				mensagem = (Message) oInput.readObject();
				LOG.info("Comando recebido " + mensagem.getCMD());
				executeCommand(mensagem);
		        oInput.close();
			} while( ! mensagem.isQuit() );
			
			LOG.info("Cliente finalizado");

		}

		/**
		 * Executa o comando em uma trhead separada
		 * @param mensagem
		 */
		private void executeCommand(Message mensagem) {
			Command cmd = CommandFactory.getCommand(mensagem);
			Thread  executeCommand = new Thread(cmd);
			executeCommand.start();
		}
	}
	
	public void start() {
	    try {

		      ServerSocket servidor = new ServerSocket(PORT);
		      
		      while(true) {
		        Socket cliente = servidor.accept();
		        
		        Thread gerenciador = new Thread(new  ClientManager( cliente) );
		        gerenciador.run();
		        
		      }  
		    }   
		    catch(Exception e) {
				LOG.info("Erro na conexao");
		    }
		    finally {
				LOG.info("Cliente finalizado");
		    	
		    }  

		
	}
}
   
