/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Middle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;


public class Vetor_BTree {

    Node root;
    TreeMap<Integer, String> newTree = new TreeMap<>();
    
    //Função para imprimir na tela o vetor que armazena os dados de ORDER ID do arquivo sample.csv
    public void AddNode() throws FileNotFoundException, IOException{
        
        BufferedReader br = null;
        String linha = " ";
        String csvDivisor = ";";
        int linhas = 0, i = 0, col = 0;
        String Continua = " ";
        String valorArvore = " ";
        
        Node novoNo = new Node();
        Node noPai = novoNo;
                    
        br = new BufferedReader(new FileReader("C:\\Users\\andre\\Desktop\\atividade pra\\Sample.csv"));
        
        while ((linha = br.readLine()) != null) {                
            linhas++;
        } 
        
        String OrderID[] = new String[linhas];
        String Nomes[]   = new String[linhas];
        int Aux[]        = new int[linhas];
        
        br = new BufferedReader(new FileReader("C:\\Users\\andre\\Desktop\\atividade pra\\Sample.csv"));
        
        col = 21;
        
        while ((linha = br.readLine()) != null) {
            
            String[] Resultado = linha.split(csvDivisor);
            
            if (i<linhas)
            {
                OrderID[i] = Resultado[1];                
                
                //preenchimento do campo de nomes com todas as colunas do arquivo (Resultado[ie] -> todas as colunas)
                for(int ie = 0; ie < col; ie++ ){
                    if (ie == 0){
                        Nomes[i] = Resultado[ie];
                    }else
                        Nomes[i] = Nomes[i] + ";" + Resultado[ie];
                }
            }
                i++;
        }  
        
        i = 1;
        while(i < linhas){
            
            Aux[i] = Integer.parseInt(OrderID[i]);
                
            newTree.put(Aux[i], Nomes[i]);
    
            noPai = novoNo;
            novoNo.key = ++noPai.key;
            i++;
        }
        
        if(root == null){
           root = novoNo;
        }
        
    }
    
    public void deleteNode(String mensagem){
        
        Node focusNode = root;
       
        int nodeToDelete = Integer.parseInt(mensagem);
        
        while(focusNode.key != nodeToDelete){
            focusNode.key++;
                        
        }
        
        newTree.remove(focusNode.key);       
        System.out.println("Deletou...");
    }
    
    public void BtreeSearch(String key){
        
        int keyToFind = Integer.parseInt(key);        
        String treeValue;

        if(newTree.containsKey(keyToFind)){
            treeValue = newTree.get(keyToFind);
            System.out.println(keyToFind + ";" + treeValue);
        }
    }
    
    
    public static void main(String[] args) throws IOException {
       Vetor_BTree imprime = new Vetor_BTree();
       imprime.AddNode(); 
       //imprime.deleteNode("56581");
       imprime.BtreeSearch("293");
    
    }
   
    //nó da btree
    class Node{
    
        private int key;
        private String name;
        
        public void Node(int key, String nome){
            key  = this.key;
            nome  = this.name;
        }
        
        @Override
          public String toString(){
		return name + " has a key " + key;
          }
        
    } 
    
}

