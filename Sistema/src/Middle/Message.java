/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Middle;

import java.io.Serializable;

/**
 *
 * @author batman
 */
public class Message implements Serializable {
	
	public static final int QUIT =  0; 
	public static final int FIND =  1;
	public static final int ADD  =  2;
	public static final int DEL  =  3;
	
	
	private int CMD;
	private Key key;
	private Data data;
	
	
	public Key getKey() {
		return key;
	}
	public void setKey(Key key) {
		this.key = key;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	
	public int getCMD() {
		return CMD;
	}
	public void setCMD(int cMD) {
		CMD = cMD;
	}
	
	public boolean isQuit() {
		return this.CMD == QUIT;
	}
	
	public boolean isFind() {
		return this.CMD == FIND;
	}
	
	public boolean isDel() {
		return this.CMD == DEL;
	}
        
        public boolean isAdd() {
		return this.CMD == ADD;
	}
}
